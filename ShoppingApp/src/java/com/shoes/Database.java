package com.shoes;

//import org.dogwood.beans.Movie;
import com.shoes.beans.Shoe;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.json.simple.JSONObject;

public class Database {

    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost:3306/shoppingapp?zeroDateTimeBehavior=convertToNull";
    private static final String USER = "root", PASSWORD = "root";
    
    public static enum Login
    {
        INCORRECT_USERNAME, INCORRECT_PASSWORD, SQL_ERROR, CORRRECT_NORMAL, CORRECT_ADMIN
    }
    private static final Database db = new Database();
    private final DataSource dataSource;
    private Connection connection;
    
    public static Database getInstance() {
        try {
            if (db.connection != null && !db.connection.isClosed()) {
                db.connection.close();
            }
            db.connection=db.dataSource.getConnection();
            return db;
        } catch (SQLException ex) {
            System.out.println(ex);
            return null;
        }
    }
    private Database() {
        dataSource = new DataSource();
        dataSource.setDriverClassName(DRIVER);
        dataSource.setUrl(URL);
        dataSource.setUsername(USER);
        dataSource.setPassword(PASSWORD);
    }
    public boolean register(String username, String email, String password, String type)
    {
        try
        {
            System.out.println("USername="+username);
            PreparedStatement statement = connection.prepareStatement("INSERT INTO USER VALUES(?, ?, MD5(?), ?)");
            statement.setString(1, username);
            statement.setString(2, email);
            statement.setString(3, password);
            statement.setString(4, type);
            statement.executeUpdate();
            connection.close();
            return true;
        }
        catch(SQLException ex)
        {
           System.out.println(ex);
           return false;
        }
        
    }
    public List<Shoe> getInStockShoes() {
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Shoe");
            ResultSet results = statement.executeQuery();
            List<Shoe> shoes = new LinkedList<Shoe>();
            while (results.next()) {
                String id = results.getString(1);
                String name = results.getString(2);
                String type = results.getString(3);
                String price = results.getString(5);
                String category = results.getString(6);
                
                shoes.add(new Shoe(id, name, type, price, category));
            }
            connection.close();
            return shoes;
        } catch (SQLException ex) {
            System.out.println(ex);
            return null;
        }
    }
    public void close() {
        try {
            connection.close();
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
    public boolean addShoe(JSONObject json) {
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO SHOE VALUES(?, ?, ?, ?, ?, ?)");
            statement.setString(1, (String) json.get("id"));
            statement.setString(2, (String) json.get("title"));
            statement.setString(3, (String) json.get("mpaa_rating"));
            statement.setString(4, (String) json.get("mpaa_rating"));
            statement.setString(5, (String) json.get("title"));
            statement.setString(6, (String) json.get("title"));
            statement.executeUpdate();
            connection.close();
            return true;
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
    }
    public Login logIn(String username, String password) {
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM User WHERE username = ?");
            statement.setString(1, username);
            if (!statement.executeQuery().next()) {
                return Login.INCORRECT_USERNAME;
            }
            statement = connection.prepareStatement(
                    "SELECT * FROM User WHERE Username = ? AND Password = MD5(?)");
            statement.setString(1, username);
            statement.setString(2, password);
            ResultSet results = statement.executeQuery();
            if (!results.next()) {
                return Login.INCORRECT_PASSWORD;
            }
            Login login = results.getString(4).equals("NORMAL") ? Login.CORRRECT_NORMAL : Login.CORRECT_ADMIN;
            connection.close();
            return login;
        } catch (SQLException ex) {
            System.out.println(ex);
            return Login.SQL_ERROR;
        }
    }
   
    public Shoe getShoeById(String id) {
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Shoe WHERE Id = ?");
            statement.setString(1, id);
            ResultSet results = statement.executeQuery();
            results.next();
            String name = results.getString(2);
            String type = results.getString(3);
            String price = results.getString(4);
            String category = results.getString(5);
            String image= results.getString(6);
            connection.close();
            return new Shoe();
        } catch (SQLException ex) {
            return null;
        }
    }
    
}

    

    

    

    