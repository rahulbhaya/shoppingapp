package com.shoes;

import java.io.InputStreamReader;
import java.net.URL;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;


public class DatabaseUploader {
public static final String API_KEY = "gs2spwmu9dt6uqnaxhsadxp6";

    public static final String BASE_URL = "http://api.rottentomatoes.com/api/public/v1.0";
    public static void main(String[] args) {
        JSONObject json;
        URL url;
        
        
        try {
            url = new URL(BASE_URL + "/lists/movies/in_theaters.json?apikey=" + API_KEY);
            json = (JSONObject) JSONValue.parse(new InputStreamReader(url.openStream()));
            JSONArray shoes = (JSONArray) json.get("movies");
            for (Object shoe : shoes) {
                Database.getInstance().addShoe((JSONObject) shoe);
            
            }
        } 
        catch (Exception ex) {
            ex.printStackTrace();
        }
        
      
        //Database.getInstance().register("TheAdmin", "Admin@Pulpkorn.com", "admin", "ADMIN");
    }

}
